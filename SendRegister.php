<?php

namespace RegisterTeste;

class SendRegister
{
    public static function sendRegister($url, $fields, $fields_string){
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data, set the returntransfer to not append true or false
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $response = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $response;
    }
}