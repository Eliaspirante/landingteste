<?php
require_once __DIR__."/Register.php";

use RegisterTeste\Register;

/** Second field validate, from post**/
$error = false;
$message = "";

if(!array_key_exists("name", $_POST)){
    $error = true;
    $message = "Por favor, utilize um nome válido!";
}

if(!array_key_exists("email", $_POST) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    $error = true;
    $message .= "Por favor, utilize um e-mail válido!";
}

if(!array_key_exists("birthdate", $_POST)){
    $error = true;
    $message .= "Por favor, utilize uma data de nascimento válido!";
}

if(!array_key_exists("genre", $_POST)){
    $error = true;
    $message .= "Por favor, selecione um género válido!";
}

if($error == true){
    $response = array(
        "code" => 500,
        "message" => $message
    );

    echo json_encode($response);
    return;
}

/*tests*/
// Return the response from post
//echo json_encode(array("code" => 200));
//return;
/*end tests*/

/** If ok, should construct the object Register and send the data */

$birthdate = explode("/",$_POST['birthdate']);

$birthdate = $birthdate[2]."-".$birthdate[1]."-".$birthdate[0];

$url = 'http://local.api.lampd.com.br/index.php/api';

try {
    $register = new Register($_POST['name'], $_POST['email'], $birthdate, $_POST['genre']);

    $response = $register->sendRegister($url);

}catch (\Exception $e){
    echo $e->getMessage();
}

// Return the response from post
echo json_encode($response);