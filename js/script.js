$( document ).ready(function() {
    $("#birthdate").mask("99/99/9999",{placeholder:"dd/mm/YYYY"});

    $("#baseform").validate({
        messages: {
            name: "Nome inválido",
            email: "E-mail inválido",
            birthdate: "Data de nascimento inválida",
        },
        submitHandler: function(form) {

            var birth = $("#birthdate").val().split("/");

            if(!verifyIfBirthDateIsValid(birth[2], birth[1], birth[0])) {
                alert("Por favor, informe uma data de nascimento válida para participar!");
                return;
            }

            if (!minimumAge(new Date(birth[2], birth[1], birth[0]), 18)) {
                alert("Desculpe, mas você deve ter 18 anos ou mais para participar!");
                return;
            }

            // Modal to block view and post to second script validation
            $(".modal").show();
            $.ajax({
                type: "POST",
                url:  "submit.php",
                dataType: "json",
                data: $("#baseform").serialize(),
                success: function(data){
                    $(".modal").hide();
                    data = JSON.parse(data);
                    if(data.code == 200) {
                        $("#baseform").hide();
                        $("#thanks").show();
                        console.log(data);
                    }else if(data.code == 500){
                        alert(data.message);
                    }else{
                        alert( data );
                    }
                },
                error: function(data){
                    $(".modal").hide();
                    alert( data );
                }
            });
            event.preventDefault();
        }
    });
});

function minimumAge(birthDate, minAge) {
    var tempDate = new Date(birthDate.getFullYear() + minAge, birthDate.getMonth(), birthDate.getDate());
    return (tempDate <= new Date());
}

function verifyIfBirthDateIsValid(year, month, day){
    var now = new Date();

    if(year <= now.getFullYear() &&
        month >= 1 && month <= 12 &&
        day >= 1 && day <= 31) {
        return true;
    }else {
        return false;
    }
}

