# README #

### This is only a simple landing page with two step verification ###

If you want to get the bower dependencies (not necessary):

* bower install

Structure of the simple landing project

```
#!php

.
├── bower.json
├── contributors.txt
├── css
│   ├── bootstrap.min.css
│   ├── index.html
│   └── style.css
├── imgs
│   ├── index.html
│   ├── loader.gif
│   └── sucesso.jpg
├── index.php
├── js
│   ├── bootstrap.min.js
│   ├── index.html
│   ├── jquery.maskedinput.min.js
│   ├── jquery.min.js
│   ├── jquery.validate.min.js
│   └── script.js
├── README.md
├── Register.php
├── SendRegister.php
└── submit.php
```


* The file index.php contains the principal form.
* The file submit.php validates the index.php post and creates a new object from Register class in Register.php file.
* Register is a class for set the data and send via SendRegister.
* SendRegister only has one method to send data via CURL POST.
* Version: 0.0.1