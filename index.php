<html xmlns="//www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <title>Landing Ganhe um IPHONE</title>

        <!-- Import css files -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <!-- End Import css files -->
    </head>
    <body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=1511325298945637&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            console.log(response);
        });
    }

    function checkAuthResponse(){
        FB.getAuthResponse(function(response){
            console.log(response);
        })
    }

    function chamada(){
        FB.api('/me', function(response) {
            console.log(JSON.stringify(response));
        });
    }

    function chamadaPaginas(){
        FB.api('/me/accounts', function(response) {
            console.log(JSON.stringify(response));
        });   
    }
</script>

<button onclick="chamada()">TESTE chamada</button>
<button onclick="chamadaPaginas()">TESTE paginas</button>
<button onclick="checkAuthResponse()">Checa response</button>
<br/>
<br/>

     <button onclick="checkLoginState()">TESTE verificacao Login</button>

<br/>
<hr>

<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>

        <hr>

        <footer class="text-center">
            <p>© 2018 Teste.</p>
        </footer>

        <div class="modal">
        </div>

        <!-- Import JS files -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <!-- End of import JS files -->
    </body>
</html>