<?php

namespace RegisterTeste;

require_once('SendRegister.php');

class Register
{
    const URL_API = "http://local.api.lampd.com.br";
    private $name;
    private $email;
    private $birthdate;
    private $genre;

    function __construct($name, $email, $birthdate, $genre){
        $this->name = $name;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->genre = $genre;
    }

    /**
     * Send register via POST CURL
     */
    public function sendRegister($url = self::URL_API){
        $fields = array(
            'name' => urlencode($this->name),
            'email' => urlencode($this->email),
            'birthdate' => urlencode($this->birthdate),
            'genre' => urlencode($this->genre),
        );

        try {
            $fields_string = "";

            //url-ify the data for the POST
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');

            return SendRegister::sendRegister($url,$fields,$fields_string);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }
}